import React from 'react';
import { Card, message, Modal } from 'antd';
import TextTruncate from 'react-text-truncate';
import { connect } from 'react-redux';
const { Meta } = Card;

function BeerItem(props) {
  // const xx = ()=>{props.onItemBeerClick(props.item)}
  return (
    <Card
      onClick={() => {
        console.log(props.onItemBeerClick);

        props.onItemBeerClick(props.item);
      }}
      hoverable
      cover={
        <div>
          <img
            src={props.item.image_url}
            style={{ height: '200px', width: 'auto', paddingTop: '16px' }}
          />
        </div>
      }
    >
      <Meta
        title={props.item.name}
        description={
          <TextTruncate
            line={2}
            truncateText="…"
            text={props.item.description}
            textTruncateChild={<a href="#">Read more</a>}
          />
        }
      />
    </Card>
  );
}

export default BeerItem;
