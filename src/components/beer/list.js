import React, { Component } from 'react';
import { List } from 'antd';
import BeerItem from './item';
import { Pagination } from 'antd';
class ListBeer extends Component {
  render() {
    console.log(this.props.onItemBeerClick);

    return (
      <div style={{ minHeight: '300px' }}>
        <List
          pagination={{
            pageSize: 12
          }}
          grid={{ gutter: 16, column: 4 }}
          dataSource={this.props.items}
          renderItem={item => (
            <List.Item>
              <BeerItem
                item={item}
                onItemBeerClick={this.props.onItemBeerClick}
              />
            </List.Item>
          )}
        />
      </div>
    );
  }
}

export default ListBeer;
